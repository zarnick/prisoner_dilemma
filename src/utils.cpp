#include "utils.h"
#include <iomanip>
#include <sys/time.h>

Utils* Utils::_instance = NULL;

Utils* Utils::instance()
{
	if(!_instance)
		_instance = new Utils();
	return _instance;
}

void Utils::resetInstance()
{
	_instance->closeDebugFile();
	_instance->closeOutputFile();
	_instance->clearPRNG();
	delete _instance;
}

void Utils::setDebug(bool debug)
{
	_debug = debug;
}

bool Utils::isDebuging()
{
	return _debug;
}

bool Utils::openDebugFile(const char *fname)
{
	_debugFS.open(fname, ios::out|ios::app);
	if(_debugFS.is_open() == false) return false;
	return true;
}

bool Utils::openOutputFile(const char *fname)
{
	_outputFS.open(fname, ios::out|ios::app);
	if(_outputFS.is_open() == false) return false;
	return true;
}

ofstream& Utils::output()
{
	if(_outputFS.is_open())
		return _outputFS;
}

ofstream& Utils::debug(bool appendInfo)
{
	if(_debugFS.is_open() && _debug==true)
	{
		if(appendInfo)
		{
			time_t curTime;
			struct tm *locTime;
			struct timeval uLocTime;

			gettimeofday(&uLocTime,NULL);
			time(&curTime);
			locTime = localtime(&curTime);

			ios::fmtflags f (_debugFS.flags());
			_debugFS << setfill('0');
			_debugFS << "[DEBUG " << 1900+locTime->tm_year << "/" << setw(2) << locTime->tm_mon+1 << "/" << setw(2) << locTime->tm_mday;
			_debugFS << " " << setw(2) << locTime->tm_hour << ":" << setw(2) << locTime->tm_min   << ":" << setw(2) << locTime->tm_sec;
			_debugFS << "." << setw(8) << uLocTime.tv_usec << "] ";
			_debugFS.flags(f);
		}
		return _debugFS;
	}
	else
	{
		if(_nullFS.is_open()==false)
			_nullFS.open("/dev/null");
		return _nullFS;
	}
}

void Utils::closeDebugFile()
{
	if(_debugFS.is_open())
		_debugFS.close();
}

void Utils::closeOutputFile()
{
	if(_outputFS.is_open())
		_outputFS.close();
}

bool Utils::startPRNG(const gsl_rng_type *prng)
{
#ifdef HAVE_LIBGSL
	gsl_rng_env_setup();
	_prngType=prng;
	if((_prng=gsl_rng_alloc(_prngType))==NULL){
		fprintf(stderr,"Utils::startPRNG() error");
		return false;
	}
	setSeed();
#endif
	return true;
}

void Utils::setSeed(unsigned long int seed)
{
#ifdef HAVE_LIBGSL
	if(seed==0)
		seed=time(NULL);
	gsl_rng_set(_prng,seed);
#endif
}

double Utils::getUniform(void)
{
	double val=0.0;
#ifdef HAVE_LIBGSL
	val=gsl_rng_uniform(_prng);
#endif
	return val;
}

unsigned long int Utils::getUniformInt(unsigned long int max)
{
	unsigned long int val=0;
#ifdef HAVE_LIBGSL
	val=gsl_rng_uniform_int(_prng,max);
#endif
	return val;
}

void Utils::clearPRNG()
{
#ifdef HAVE_LIBGSL
	if(_prng!=NULL)
		gsl_rng_free(_prng);
#endif
}
