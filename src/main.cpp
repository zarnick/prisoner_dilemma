#include "utils.h"
#include "ga.h"
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>


using namespace std;

const char *progname;

void help(FILE *stream, int errCode)
{
	fprintf(stream,"Usage: %s [options]\n",progname);
	fprintf(stream,"\t-h\t--help\t\t\t\tThis help message.\n");
	fprintf(stream,"\t-d\t--debug\t\tfile\t\tSet debug file [default:'debug.log'].\n");
	fprintf(stream,"\t-o\t--output\tfile\t\tSet output file [default:'/dev/stdout'].\n");
	fprintf(stream,"\t-v\t--verbose\t\t\tSet verbosity on [default: false].\n");
	fprintf(stream,"\t-g\t--generation\tgenerations\tSet amount of generation [default:100].\n");
	fprintf(stream,"\t-p\t--population\tpopulation\tSet size of population [default:100].\n");
	fprintf(stream,"\t-t\t--history\thistory\t\tSet amount of history runs [default:10].\n");
	fprintf(stream,"\t-e\t--elite\t\telite\t\tSet percent of elite [default:0.2].\n");
	fprintf(stream,"\t-m\t--mutate\tmutate\t\tSet percent of mutation [default:0.03].\n");
	fprintf(stream,"\n");
	fprintf(stream,"Example: %s -d \"debug.log\" -v -g 100 -p 100 -h 10 -e 0.5 -m 0.05\n",progname);
	exit(errCode);
}

int main(int argc, char *argv[])
{
	progname = argv[0];
	unsigned int g=100;
	unsigned int p=100;
	unsigned int h=30;
	double e=0.2;
	double m=0.03;
	bool debug=false;

	//Parse parameters
	int nextOption;
	const char * const shortOptions="hd:o:vg:p:t:e:m:";
	const struct option longOptions[]=
	{
		{"help",0,NULL,'h'},
		{"debug",1,NULL,'d'},
		{"output",1,NULL,'o'},
		{"verbose",0,NULL,'v'},
		{"generations",1,NULL,'g'},
		{"population",1,NULL,'p'},
		{"history",1,NULL,'t'},
		{"elite",1,NULL,'e'},
		{"mutate",1,NULL,'m'},
		{NULL,0,NULL,0}
	};

	const char *outputFname=NULL;
	const char *debugFname=NULL;
	do{
		nextOption = getopt_long(argc,argv, shortOptions,longOptions,NULL);
		switch(nextOption)
		{
			case 'h': help(stdout,0); break;
			case '?': help(stderr,1); break;
			case 'd': debugFname=optarg; break;
			case 'o': outputFname=optarg; break;
			case 'v': debug=true;break;
			case 'g': g=atoi(optarg); break;
			case 'p': p=atoi(optarg); break;
			case 't': h=atoi(optarg); break;
			case 'e': e=atof(optarg); break;
			case 'm': m=atof(optarg); break;
		}
	}while(nextOption!=-1);

	if(outputFname!=NULL)	Utils::instance()->openOutputFile(outputFname);
	else Utils::instance()->openOutputFile("/dev/stdout");
	if(debugFname!=NULL) Utils::instance()->openDebugFile(debugFname);
	else Utils::instance()->openDebugFile("debug.log");
	Utils::instance()->setDebug(debug);
	Utils::instance()->debug() << "-====PRISONER DILLEMA===-\n";
	Utils::instance()->debug() << "[*] Starting a new run wih parameters:\n";
	Utils::instance()->debug() << "[*] Generations: " << g << '\n';
	Utils::instance()->debug() << "[*] Population : " << p << '\n';
	Utils::instance()->debug() << "[*] History    : " << h << '\n';
	Utils::instance()->debug() << "[*] Elite      : " << e << '\n';
	Utils::instance()->debug() << "[*] Mutation   : " << m << '\n';
	Utils::instance()->output() << "[*] Starting PRNG\n";
	Utils::instance()->debug() << "[*] Starting PRNG\n";
	Utils::instance()->startPRNG();
	GA *pd = new GA(g,p,h,e,m);
	pd->run();
	delete pd;

	Utils::instance()->resetInstance();
	return 0;
}
