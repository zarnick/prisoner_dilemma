#include "prisoner.h"

Prisoner::Prisoner(unsigned int h)
{
	_history=h;
	_fitness=0;
}

void Prisoner::addStrategy(unsigned int strategy)
{
	_strategy.push_back(strategy);
}

void Prisoner::setStrategy(unsigned int h, unsigned int strategy)
{
	_strategy[h]=strategy;
}

unsigned int Prisoner::strategy(unsigned int h)
{
	return _strategy[h];
}

unsigned int Prisoner::execStrategy(unsigned int h,Prisoner* a)
{
	unsigned int p1Strat=_strategy[h];
	unsigned int p2Strat=a->strategy(h);
	if(p1Strat==COOPERATE && p2Strat==COOPERATE) return 3;
	if(p1Strat==COOPERATE && p2Strat==DEFECT) return 0;
	if(p1Strat==DEFECT && p2Strat==COOPERATE) return 5;
	if(p1Strat==DEFECT && p2Strat==DEFECT) return 1;
	return 0;
}

void Prisoner::setFitness(unsigned int f)
{
	_fitness=f;
}

unsigned int Prisoner::fitness()
{
	return _fitness;
}

void Prisoner::swap(unsigned int h)
{
	_strategy[h]=(_strategy[h]==COOPERATE?DEFECT:COOPERATE);
}
