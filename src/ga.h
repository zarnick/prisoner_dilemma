#ifndef __GA_H
#define __GA_H

#include "prisoner.h"

#include <vector>

using namespace std;

/**
 * @brief The Genectic Algorithm class.
 * Here is where the GA is actually run and has all it's logic.\n
 * This version is related to 2 players evolving at the same time, trying to find a common place where both of them can win.\n
 * At each generation, we have p strategies running against each other (player1 strategy1 X player2 strategy1, player1 strategy2 X player2 strategy2, ...).
 * After all strategies have been tested, the fitness of each strategy is the average of the fitness of player1 and player2. Then we have a very
 * simple Cross Over, where we keep the e Elite strategies, and remove all the others (again, on each player) and each new strategy is created from
 * one part of the 1st strategy and another part of the 2nd strategy. Example:\n
 * - Strategy 1: CCDCDDCDCC
 * - Strategy 2: DDCCDCDDDD
 * - Xover point: 3
 * - Result Strategy 1: CCDCDCDDDD
 * - Result Strategy 2: DDCCDDCDCC
 * And when we are creating the new Strategy, each history option (COOPERATE or DEFECT) has a m chance of mutate (ie: swapping).\n
 * After this, the test goes all over again.\n
 * In other words, here's what's being done:
 * -# Create p strategies for player 1 and player 2
 * -# For each generation g:
 *		-#	Calculate fitness
 *		-#	XOver
 *		-#	Mutate
 * -# Show best solutions
 * Where the "Calculate fitness" part is:
 * -#	For each strategy s:
 *	-#	Execute strategy h on player1 against player2
 *	-#	Execute strategy h on player2 against player1
 *	-#	Set fitness on player1 and player2 as (fitness1+fitness2)/2 * 
 */
class GA
{
	public:
		/**
		 * @brief The default constructor
		 * @param g The amount of generations.
		 * @param p The amount of strategies (the Population).
		 * @param h The size of each strategy (the History).
		 * @param e The amount of Elite strategies (Elite=p*e).
		 * @param m The probability of mutation.
		 */
		GA(unsigned int g=100, unsigned int p=100, unsigned int h=10, double e=0.2, double m=0.03);
		/**
		 * @brief Default destructor, clears all memory.
		 */
		~GA();
		/**
		 * @brief Execute the GA.
		 */
		void run();
	private:
		/**
		 * @brief Create the population of strategies.
		 * @param id The population id (0,1)
		 */
		void _createPopulation(unsigned int id);
		void _calcFitness(); ///Find the fitness for each player
		/**
		 * @brief Debugs the full population on the debug output.
		 * @param id The population id (0,1)
		 */
		void _debugPopulation(unsigned int id);
		void _crossover(); ///Execute the crossover
		void _mutate(Prisoner *p); ///Mutate each strategy

		unsigned int _generations;
		unsigned int _popSize;
		unsigned int _history;
		unsigned int _elite;
		double _probMutate;

		vector<Prisoner*> _population1;
		vector<Prisoner*> _population2;
};

#endif //__GA_H
