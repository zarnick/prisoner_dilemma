#ifndef __UTILS_H
#define __UTILS_H

#include "config.h"
#include <fstream>
#ifdef HAVE_LIBGSL
#include <gsl/gsl_rng.h>
#endif

using namespace std;

/**
 * @brief A simple generic Utility class.
 * This is a very simple singleton class. It consists of some set-up and some cleanup, mainly:
 * - Setup:
 *	- openDebugFile()
 *	- openOutputFile()
 *	- startPRNG() (If needed)
 * - Cleanup:
 *	- resetInstance()
 *
 * @author: Mateus "zarnick" Interciso
 * @version 1.0.0
 */
class Utils{
	public:
		static Utils* instance(); ///Returns a pointer to the singleton instance
		static void resetInstance(); ///Resets the instance

		/**
		 * @brief Sets if we are debugging or not.
		 * @param debug If true, we will write on the debug device
		 */
		void setDebug(bool debug);
		/**
		 * @brief Returns if its debugging or not.
		 * @return True if debugging, false otherwise.
		 */
		bool isDebuging(); 

		/**
		 * @brief Opens the debug file for writting, appending at the end.
		 * @param fname The file name to open.
		 * @return True if file was opened sucessfully, false otherwise.
		 */
		bool openDebugFile(const char *fname);
		/**
		 * @brief Opens the output file for writting, appending at the end.
		 * @param fname The file name to open.
		 * @return True if file was opened sucessfully, false otherwise.
		 */
		bool openOutputFile(const char *fname);
		/**
		 * @brief Returns the output file stream.
		 * @return The actual output fstream to write on.
		 */
		ofstream& output();
		/**
		 * @brief Returns the debug file stream.
		 * @return The actual debug fstream to write on.
		 */
		ofstream& debug(bool appendInfo=true);
		void closeDebugFile(); ///Closes the debug file.
		void closeOutputFile(); ///Closes the output file

		/**
		 * @param Allocate and starts the GSL PRNG.
		 * @param prng The GSL type
		 * @return True if sucessfull, false otherwise.
		 */
		bool startPRNG(const gsl_rng_type *prng=gsl_rng_default);
		/**
		 * @brief Sets a seed, if needed.
		 * @param seed The seed to set to.
		 */
		void setSeed(unsigned long int seed=0);
		/**
		 * @brief Returns a random value from [0.0-1.0[, using a uniform sample
		 * @return A random value from [0.0-1.0]
		 */
		double getUniform(void);
		/**
		 * @brief Return a random value from [1-max[, using a uniform sample.
		 * @param max The max value to return.
		 * @return The random value from [1-max[
		 */
		unsigned long int getUniformInt(unsigned long int max);
		void clearPRNG(); ///Removes the memory from the PRNG

	private:
		Utils(){};
		Utils(const Utils&){};
		static Utils *_instance;

		bool _debug;
		ofstream _outputFS;
		ofstream _debugFS;
		ofstream _nullFS;

#ifdef HAVE_LIBGSL
		const gsl_rng_type *_prngType;
		gsl_rng *_prng;
#endif
};
#endif
