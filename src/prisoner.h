#ifndef __PRISONER_H
#define __PRISONER_H

#define COOPERATE 0
#define DEFECT 1

#include <vector>

using namespace std;

/**
 * @brief The Prisoner class
 * This is the actual player definition. Each player has a strategy that can go up until history
 * times (ie: they can "play the game" history times).\n
 * How well the strategy goes trough time, is the actuall goal of the game.
 * @author Mateus "Zarnick" Interciso
 */
class Prisoner
{
	public:
		/**
		 * @brief The default constructor.
		 * @param h How much of history should we be playing.
		 */
		Prisoner(unsigned int h);
		/**
		 * @brief Adds a strategy to the player
		 * @param strategy The strategy (either COOPERATE or DEFECT)
		 */
		void addStrategy(unsigned int strategy);
		/**
		 * @brief Reset a strategy.
		 * @param h The strategy id (<_history).
		 * @param strategy The strategy to be set (COOPERATE or DEFECT)
		 */
		void setStrategy(unsigned int h, unsigned int strategy);
		/**
		 * @brief Returns the strategy at history h
		 * @param h The history id
		 * @return The strategy (COOPERATE or DEFECT)
		 */
		unsigned int strategy(unsigned int h);
		/**
		 * @brief Execute the strategy at history h
		 * @param h The history to execute the strategy at.
		 * @param a The second player to execute the strategy on.
		 * @return The value of the strategy, according to the Prisonner Dilema table.
		 */
		unsigned int execStrategy(unsigned int h, Prisoner* a);
		/**
		 * @brief Sets the fitness of the player
		 * @param f The fitness (>0)
		 */
		void setFitness(unsigned int f);
		/**
		 * @brief Return the fitness of the player
		 * @return The fitness of the player.
		 */
		unsigned int fitness();
		/**
		 * @brief Swaps the strategy at history h
		 * @param h The history id to swap the strategy.
		 */
		void swap(unsigned int h);
	private:
		vector<unsigned int> _strategy;
		unsigned int _history;
		unsigned int _fitness;
};

#endif //__PRISONER_H
