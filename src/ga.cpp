#include "ga.h"
#include "utils.h"
#include <algorithm>

bool sortPrisoners(Prisoner *a, Prisoner *b)
{
	return a->fitness() > b->fitness();
}

GA::GA(unsigned int g, unsigned int p, unsigned int h, double e, double m)
{
	_popSize=p;
	_generations =g;
	_history=h;
	_elite = _popSize*e;
	_probMutate = m;
	_createPopulation(0);
	_createPopulation(1);
}

GA::~GA()
{
	for(int i=0;i<_population1.size();i++)
		delete _population1[i];
	_population1.clear();
	for(int i=0;i<_population2.size();i++)
		delete _population2[i];
	_population2.clear();
}

void GA::run()
{
	if(Utils::instance()->isDebuging())
	{
		Utils::instance()->debug() << "-===================--\n";
		Utils::instance()->debug() << "Population BEFORE GA:\n";
		Utils::instance()->debug() << "Population 1:\n";_debugPopulation(0);
		Utils::instance()->debug() << "Population 2:\n";_debugPopulation(1);
	}
	for(int g=0;g<_generations;g++)
	{
		Utils::instance()->debug() << "Generation " << g << '\n';
		_calcFitness();
		sort(_population1.begin(), _population1.end(), sortPrisoners);
		sort(_population2.begin(), _population2.end(), sortPrisoners);
		if(g!=_generations-1)
			_crossover();
		if(Utils::instance()->isDebuging())
		{
			Utils::instance()->debug() << "Population 1:\n";_debugPopulation(0);
			Utils::instance()->debug() << "Population 2:\n";_debugPopulation(1);
		}
	}
	if(Utils::instance()->isDebuging())
	{
		Utils::instance()->debug() << "Population AFTER GA:\n"; 
		Utils::instance()->debug() << "Population 1:\n";_debugPopulation(0);
		Utils::instance()->debug() << "Population 2:\n";_debugPopulation(1);
	}
	Utils::instance()->output() << "Best strategy:\n";
	Utils::instance()->output() << "Player 1:";
	for(int h=0;h<_history;h++)
		Utils::instance()->output() << "(" << (_population1[0]->strategy(h)==COOPERATE?'C':'D') << ")";
	Utils::instance()->output() << "{" << _population1[0]->fitness() << "}\n";
	Utils::instance()->output() << "Player 2:";
	for(int h=0;h<_history;h++)
		Utils::instance()->output() << "(" << (_population2[0]->strategy(h)==COOPERATE?'C':'D') << ")";
	Utils::instance()->output() << "{" << _population2[0]->fitness() << "}\n";
}

void GA::_createPopulation(unsigned int id)
{
	if(id==0)
	{
		for(int i=0;i<_popSize;i++)
		{
			Prisoner *p = new Prisoner(_history);
			for(int j=0;j<_history;j++)
				p->addStrategy(Utils::instance()->getUniformInt(DEFECT+1));
			_population1.push_back(p);
		}
	}else
	{
		for(int i=0;i<_popSize;i++)
		{
			Prisoner *p = new Prisoner(_history);
			for(int j=0;j<_history;j++)
				p->addStrategy(Utils::instance()->getUniformInt(DEFECT+1));
			_population2.push_back(p);
		}
	}
}

void GA::_calcFitness()
{
	unsigned int fitness1;
	unsigned int fitness2;
	for(int p=0;p<_popSize;p++)
	{
		fitness1=0;
		fitness2=0;
		_population1[p]->setFitness(0);
		_population2[p]->setFitness(0);
		for(int h=0;h<_history;h++){
			fitness1+=_population1[p]->execStrategy(h,_population2[p]);
			fitness2+=_population2[p]->execStrategy(h,_population1[p]);
		}
		_population1[p]->setFitness((fitness1+fitness2)/2);
		_population2[p]->setFitness((fitness1+fitness2)/2);
	}
}

void GA::_debugPopulation(unsigned int id)
{
	Prisoner *p=NULL;
	for(int i=0;i<_popSize;i++)
	{
		p=(id==0?_population1[i]:_population2[i]);
		Utils::instance()->debug(false) << "[*] P[" << i << "]:";
		for(int j=0;j<_history;j++)
			Utils::instance()->debug(false) << "(" << (p->strategy(j)==COOPERATE?'C':'D') << ")";
		Utils::instance()->debug(false) << " {" << p->fitness() << "}\n";
	}
}

void GA::_crossover()
{
	Prisoner *p1,*p2,*s1,*s2;
	unsigned int p1Id,p2Id;
	unsigned int xopoint;
	//First remove the prisoners with low fitness
	for(int i=_elite;i<_popSize;i++){
		delete _population1[i];
		delete _population2[i];
	}
	_population1.erase(_population1.begin()+_elite,_population1.end());
	_population2.erase(_population2.begin()+_elite,_population2.end());
	//Now, create the _popSize-_elite population from the crossover of the _elite population
	//First for the player 1 (population 1)
	for(int i=0;i<_popSize-_elite;i+=2)
	{
		p1Id = Utils::instance()->getUniformInt(_elite);
		p2Id = Utils::instance()->getUniformInt(_elite);
		xopoint = Utils::instance()->getUniformInt(_history);
		p1=_population1[p1Id];
		p2=_population1[p2Id];
		s1 = new Prisoner(_history);
		s2 = new Prisoner(_history);
		for(int j=0;j<xopoint;j++)
		{
			s1->addStrategy(p2->strategy(j));
			s2->addStrategy(p1->strategy(j));
		}
		for(int j=xopoint;j<_history;j++)
		{
			s1->addStrategy(p1->strategy(j));
			s2->addStrategy(p2->strategy(j));
		}
		_mutate(s1);
		_mutate(s2);
		_population1.push_back(s1);
		_population1.push_back(s2);
	}
	//Then the same thing for player 2 (population 2)
	for(int i=0;i<_popSize-_elite;i+=2)
	{
		p1Id = Utils::instance()->getUniformInt(_elite);
		p2Id = Utils::instance()->getUniformInt(_elite);
		xopoint = Utils::instance()->getUniformInt(_history);
		p1=_population2[p1Id];
		p2=_population2[p2Id];
		s1 = new Prisoner(_history);
		s2 = new Prisoner(_history);
		for(int j=0;j<xopoint;j++)
		{
			s1->addStrategy(p2->strategy(j));
			s2->addStrategy(p1->strategy(j));
		}
		for(int j=xopoint;j<_history;j++)
		{
			s1->addStrategy(p1->strategy(j));
			s2->addStrategy(p2->strategy(j));
		}
		_mutate(s1);
		_mutate(s2);
		_population2.push_back(s1);
		_population2.push_back(s2);
	}

}

void GA::_mutate(Prisoner *p)
{
	for(int h=0;h<_history;h++)
	{
		if(Utils::instance()->getUniform() < _probMutate)
			p->swap(h);
	}
}
